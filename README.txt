INTRODUCTION
------------
Form Keybinds allows module developers to bind keypress events to form API
elements in custom forms.

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/form_api_keybinds


REQUIREMENTS
------------
This module requires the following modules:
 * Libraries API (https://drupal.org/project/libraries)

This module also requires the following third party libraries:
 * Mousetrap (https://github.com/ccampbell/mousetrap)


INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

 * Download the Mousetrap JavaScript library and place the mousetrap.js file
   in sites/all/libraries/mousetrap.


CONFIGURATION
-------------
 * To utilize keybinds on your button or submit elements, you just supply a
   #key_binds key in your element's definition array. The value of this key
   is an array of keyboard keys that can be pressed to trigger the element's
   click/submit action. Please refer to the Mousetrap documentation for allowed
   values: https://craig.is/killing/mice.
