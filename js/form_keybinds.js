(function ($) {
  Drupal.behaviors.formKeybinds = {
    attach: function (context, settings) {
      for (var elementId in settings.formKeybinds) {
        if (settings.formKeybinds.hasOwnProperty(elementId)) {
          var keyBinds = settings.formKeybinds[elementId];

          (function (elementId, keyBinds) {
            Mousetrap.bind(keyBinds, function (e) {
              $('#' + elementId)
                .click()
                .mousedown();

            });
          } (elementId, keyBinds));
        }
      }
    }
  };
} (jQuery));
